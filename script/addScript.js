window.addEventListener('keypress', function(e) {
  if (e.ctrlKey && e.keyCode == 17) {
    addComment();
  }
}, false);

function addComment () {
  let comment = document.getElementsByClassName('addComment-input')[0].value;
  if( comment !== '' ) {
    let blockComment = document.getElementsByClassName('comment')[0].children[1];
    blockComment.remove();
    let commentOne = document.createElement('div');
    commentOne.classList.add('comment-one');

    let commentOneName = document.createElement('p');
    commentOneName.innerText = 'Аноним';
    commentOneName.classList.add('comment-one-name');
    commentOne.appendChild(commentOneName);

    let commentOneDate = document.createElement('p');
    commentOneDate.innerText = '12 апреля 2018';
    commentOneDate.classList.add('comment-one-date');
    commentOne.appendChild(commentOneDate);

    let commentOneDeg = document.createElement('div');
    commentOneDeg.classList.add('comment-one-deg');
    commentOne.appendChild(commentOneDeg);

    let commentOneDegDeg = document.createElement('div');
    commentOneDegDeg.classList.add('comment-one-deg-deg');
    commentOne.appendChild(commentOneDegDeg);

    let commentOneCommnet = document.createElement('p');
    commentOneCommnet.innerText = comment;
    commentOneCommnet.classList.add('comment-one-commnet');
    commentOne.appendChild(commentOneCommnet);

  
    let list =  document.getElementsByClassName('comment')[0];
    list.insertBefore(commentOne, list.lastElementChild);

    document.getElementsByClassName('addComment-input')[0].value = '';
  } else {
    alert('Введите отзыв.');
  }
}